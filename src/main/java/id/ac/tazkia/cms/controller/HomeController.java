package id.ac.tazkia.cms.controller;
import id.ac.tazkia.cms.constants.Status;
import id.ac.tazkia.cms.dao.NewsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class HomeController {
    @Autowired
    private NewsDao newsDao;

    @GetMapping("/")
    public String homePage(Model model, Authentication authentication, @PageableDefault(value = 8) Pageable pageable) {
        model.addAttribute("newsList", newsDao.findByStatusOrderByPublishDateDesc(Status.PUBLISHED,pageable));
        model.addAttribute("linkActive","home");

        return "home/index";
    }
}
