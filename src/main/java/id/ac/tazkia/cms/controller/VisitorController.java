package id.ac.tazkia.cms.controller;

import id.ac.tazkia.cms.dao.VisitorsDao;
import id.ac.tazkia.cms.entity.User;
import id.ac.tazkia.cms.entity.Visitors;
import id.ac.tazkia.cms.service.UserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@Slf4j
public class VisitorController {
    @Autowired
    private VisitorsDao visitorsDao;
    @Autowired
    private UserService userService;

    @GetMapping("/visitor")
    public String saveVisitor(@Valid Visitors visitors, RedirectAttributes attributes) {
        visitorsDao.save(visitors);
        attributes.addFlashAttribute("success", "Save Data Success");
        return "redirect:#contact";

    }

    @GetMapping("/inbox")
    public String messagesPage(Model model, @RequestParam(required = false) String search,
                               Authentication authentication, @RequestParam(defaultValue = "10") int pageSize,
                               @RequestParam(defaultValue = "0") int page) {
        User user = userService.checkAuthenticationType(authentication);
        Pageable pageable = PageRequest.of(page,pageSize);

        model.addAttribute("pageSize", pageSize);
        model.addAttribute("user", user);
        if (StringUtils.hasText(search)){
            model.addAttribute("search", search);
            model.addAttribute("visitorList", visitorsDao.findVisitorsWithSearch(search,pageable));
        }else {
            model.addAttribute("visitorList", visitorsDao.findByDeletedIsNullOrderByCreatedDesc(pageable));
        }
        return "inbox/index";
    }


}
