package id.ac.tazkia.cms.controller;
import id.ac.tazkia.cms.dao.UserDao;
import id.ac.tazkia.cms.entity.User;
import id.ac.tazkia.cms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class DashboardController {
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserService userService;

    @GetMapping("/dashboard")
    public String dashboardPage(Model model, Authentication authentication) {
        User user = userService.checkAuthenticationType(authentication);

        model.addAttribute("user", user);
        model.addAttribute("linkActive","dashboard");

        return "dashboard/index";
    }
}
