package id.ac.tazkia.cms.controller;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class StudyProgramController {
    
    @GetMapping("/program-studi/teknik-informatika")
    public String teknikInformatikaPage(Model model, Authentication authentication) {
        
        model.addAttribute("linkActive","teknikInformatika");

        return "studyprogram/teknikInformatika";
    }

    @GetMapping("/program-studi/sistem-informasi")
    public String sistemInfromasipageString(Model model, Authentication authentication) {
          model.addAttribute("linkActive","sistemInformasi");
        return "studyprogram/sistemInformasi";
    }
    
}
