package id.ac.tazkia.cms.controller;
import id.ac.tazkia.cms.constants.Status;
import id.ac.tazkia.cms.dao.NewsDao;
import id.ac.tazkia.cms.entity.News;
import id.ac.tazkia.cms.entity.User;
import id.ac.tazkia.cms.service.NewsService;
import id.ac.tazkia.cms.service.UserService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Controller
@Slf4j
public class NewsController {
    @Value("${base.url.prod}")
    private String baseUrl;
    @Autowired
    private UserService userService;
    @Autowired
    private NewsService newsService;
    @Autowired
    private NewsDao newsDao;

    @GetMapping("/news/list")
    public String dashboardPage(Model model, @RequestParam(required = false) String search,
                                @RequestParam(required = false) String status,
                                Authentication authentication,
                                @RequestParam(defaultValue = "10") int pageSize,
                                @RequestParam(defaultValue = "0") int page) {
        User user = userService.checkAuthenticationType(authentication);
        Pageable pageable = PageRequest.of(page, pageSize);
        status = StringUtils.hasText(status) ? status : "PUBLISHED";
        if(search != null){
            model.addAttribute("search", search);
        }
        model.addAttribute("status", status);
        model.addAttribute("newsList", StringUtils.hasText(search)
                ? newsDao.findNewsWithSearchAndFilter(search, Status.valueOf(status), pageable)
                : newsDao.findByStatusOrderByPublishDateDesc(Status.valueOf(status), pageable));
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("user", user);
        model.addAttribute("linkActive","newsList");

        return "news/list";
    }

    @GetMapping("/news/create")
    public String createNewsPage(@RequestParam(required = false) String id, Model model, Authentication authentication) {
        User user = userService.checkAuthenticationType(authentication);
        model.addAttribute("user", user);
        model.addAttribute("newsStatus", Status.values());
        model.addAttribute("news", StringUtils.hasText(id) ? newsDao.findById(id).orElse(new News()) : new News());
        model.addAttribute("title", StringUtils.hasText(id) ? "Edit News" :"Add News");
        model.addAttribute("linkActive", StringUtils.hasText(id) ? "newsEdit" :"newsCreate");
        return "news/create";
    }

    @PostMapping("/news/create-news")
    public String postNews(@Valid News news, Authentication authentication) {
        User user = userService.checkAuthenticationType(authentication);
        News n = news.getId().isEmpty() ? newsService.saveNews(news, user) : newsService.updateNews(news, user);
        return "redirect:list?status=" + n.getStatus();
    }

    @GetMapping("/news")
    public String newsPage(Model model,@RequestParam(defaultValue = "10") int pageSize,
                           @RequestParam(defaultValue = "0") int page) {
        Pageable pageable = PageRequest.of(page,pageSize);
        model.addAttribute("newsList", newsDao.findByStatusOrderByPublishDateDesc(Status.PUBLISHED,pageable));
        model.addAttribute("linkActive", "news");
        return "news/index";
    }

    @GetMapping("/news/{slug}")
    public String newsDetailPage(@PathVariable String slug, Model model) {
        newsDao.findBySlug(slug).ifPresent(news -> {
            model.addAttribute("news", news);
            model.addAttribute("baseUrl", baseUrl);
        });
        return "news/detail";
    }

    @GetMapping("/news/trashed")
    public String moveToTrash(@RequestParam String id) {
        newsDao.findById(id).ifPresentOrElse(news -> {
            news.setStatus(Status.TRASHED);
            newsDao.save(news);
        }, () -> log.info("ID Not Found"));
        return "redirect:list";
    }

    @GetMapping("/news/restore")
    public String restoreNews(@RequestParam String id) {
        newsDao.findById(id).ifPresentOrElse(news -> {
            news.setStatus(Status.DRAFTS);
            newsDao.save(news);
        }, () -> log.info("ID Not Found"));
        return "redirect:list";
    }

    @GetMapping("/news/delete")
    public String deleteNews(@RequestParam String id) {
        newsDao.findById(id).ifPresentOrElse(newsDao::delete, () -> log.info("ID Not Found"));
        return "redirect:list";
    }
    
    
    
}
