package id.ac.tazkia.cms.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LandingPageController {
    @GetMapping("/lp/regular")
    public String landingPage() {
        return "landingpage/regular";
    }
}
