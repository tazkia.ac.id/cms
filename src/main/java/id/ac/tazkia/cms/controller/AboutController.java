package id.ac.tazkia.cms.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
public class AboutController {

    @GetMapping("/about")
    public String homePage(Model model) {
       
        model.addAttribute("linkActive","about");

        return "about/index";
    }
}
