package id.ac.tazkia.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;

import id.ac.tazkia.cms.dao.UserDao;
import id.ac.tazkia.cms.entity.User;
import id.ac.tazkia.cms.service.UserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class KipController {
    
    @Autowired
    private UserService userService;
    @GetMapping("/kipkuliah")
    public String kipKuliahPage() {
        return "kip/index";
    }

    @GetMapping("/kipkuliah/list")
    public String kipKuliahListPage(Model model, Authentication authentication) {
        User user = userService.checkAuthenticationType(authentication);

        model.addAttribute("user", user);
        model.addAttribute("linkActive","kipKuliah");

        return "kip/list";
    }

    @GetMapping("/kipkuliah/detail")
    public String kipKuliahDetailPage(Model model, Authentication authentication) {
        User user = userService.checkAuthenticationType(authentication);

        model.addAttribute("user", user);
        model.addAttribute("linkActive","kipKuliah");
        
        return "kip/detail";
    }

     @GetMapping("/kipkuliah/periode")
    public String kipKuliahPeriodePage(Model model, Authentication authentication) {
        User user = userService.checkAuthenticationType(authentication);

        model.addAttribute("user", user);
        model.addAttribute("linkActive","kipPeriodesList");
        
        return "kip/periode";
    }

    
}
