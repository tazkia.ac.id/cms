package id.ac.tazkia.cms.controller;
import id.ac.tazkia.cms.dao.RoleDao;
import id.ac.tazkia.cms.dao.UserDao;
import id.ac.tazkia.cms.dao.UserPasswordDao;
import id.ac.tazkia.cms.dto.user.UserDto;
import id.ac.tazkia.cms.entity.User;
import id.ac.tazkia.cms.service.UserService;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;
import java.util.UUID;


@Controller
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private UserPasswordDao userPasswordDao;

    @GetMapping("/user/list")
    public String usersPage(Model model,Authentication authentication, @RequestParam(required = false) String search,
                            @RequestParam(defaultValue = "10") int pageSize,
                            @RequestParam(defaultValue = "0") int page) {
        User user = userService.checkAuthenticationType(authentication);
        Pageable pageable = PageRequest.of(page,pageSize);

        model.addAttribute("pageSize", pageSize);
        model.addAttribute("user", user);
        if (StringUtils.hasText(search)){
            model.addAttribute("search", search);
            model.addAttribute("userList", userDao. findUsersWithSearch(search,pageable));
        }else {
            model.addAttribute("userList", userDao.findByActive(true,pageable));
        }

        model.addAttribute("linkActive","usersList");

        return "user/index";
    }

    @GetMapping("/user/create")
    public String getUserPage(Model model, Authentication authentication, @RequestParam(required = false) String id) {
        User user = userService.checkAuthenticationType(authentication);
        model.addAttribute("user", user);

        if (StringUtils.hasText(id)){
            Optional<User> findUser = userDao.findById(id);
            if (findUser.isEmpty()){
                log.debug("user id {} not found ", id);
            }
            UserDto userDto = new UserDto();
            BeanUtils.copyProperties(findUser.get(), userDto);
            userDto.setPassword(userPasswordDao.findByUser(findUser.get()).getPassword());
            userDto.setRole(findUser.get().getRole().getId());
            model.addAttribute("createUser", userDto);
        }else {
            model.addAttribute("createUser", new UserDto());
        }

         model.addAttribute("title", StringUtils.hasText(id) ? "Edit User" :"Add User");
        model.addAttribute("linkActive", StringUtils.hasText(id) ? "userEdit" :"userCreate");

        return "user/create";
    }

    @PostMapping("/user/create")
    @Transactional
    public String createUser(@Valid UserDto userDto){
        if (userDto.getId().isEmpty()){
            userService.createUser(userDto);
        }else {
            userService.updateUser(userDto);
        }
        return "redirect:list";
    }

    @PostMapping("/user/delete")
    @Transactional
    public String deleteUser(@RequestParam String id){
        User user = userDao.findById(id).get();
        user.setActive(false);
        userDao.save(user);
        return "redirect:list";
    }
    
}
