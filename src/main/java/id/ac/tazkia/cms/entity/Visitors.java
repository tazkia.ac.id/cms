package id.ac.tazkia.cms.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Data
@Entity
@SQLDelete(sql = "UPDATE visitors SET deleted=now() WHERE id=?")
@Where(clause = "deleted IS NULL")
public class Visitors extends BaseEntity {
    private String name;
    @Column(unique = true)
    private String email;
    @Column(unique = true)
    private String phone;
    @Column(columnDefinition = "LONGTEXT")
    private String message;
}
