package id.ac.tazkia.cms.entity;

import id.ac.tazkia.cms.constants.Status;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Entity
@Data
@SQLDelete(sql = "UPDATE news SET deleted=now() WHERE id=?")
@Where(clause = "deleted IS NULL")
public class News extends BaseEntity{
    @NotNull
    @Column(columnDefinition = "LONGTEXT")
    private String thumbnail;
    @NotNull
    @Column(columnDefinition = "LONGTEXT")
    private String title;
    private String subtitle;
    @Column(columnDefinition = "LONGTEXT")
    private String slug;
    @NotNull
    @Column(columnDefinition = "LONGTEXT")
    private String content;
    @NotNull
    private String author;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(columnDefinition = "DATE")
    private LocalDate publishDate = LocalDate.now();

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "VARCHAR")
    private Status status = Status.DRAFTS;
}
