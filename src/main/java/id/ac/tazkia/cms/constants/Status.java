package id.ac.tazkia.cms.constants;

public enum Status {
    PUBLISHED("Berita sudah dipublish"),
    DRAFTS("Berita di draft"),
    TRASHED("Berita di hapus");

    private final String getValueStatus;

    Status(String getValueStatus) {
        this.getValueStatus = getValueStatus;
    }

    public String getDisplayName() {
        return getValueStatus;
    }
    }

