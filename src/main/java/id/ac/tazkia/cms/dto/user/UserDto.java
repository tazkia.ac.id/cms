package id.ac.tazkia.cms.dto.user;

import lombok.Data;

@Data
public class UserDto {
    private String id;
    private String name;
    private String username;
    private String email;
    private String password;
    private String role;
}
