package id.ac.tazkia.cms.service;

import id.ac.tazkia.cms.dao.RoleDao;
import id.ac.tazkia.cms.dao.UserDao;
import id.ac.tazkia.cms.dao.UserPasswordDao;
import id.ac.tazkia.cms.dto.user.UserDto;
import id.ac.tazkia.cms.entity.User;
import id.ac.tazkia.cms.entity.UserPassword;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private UserPasswordDao userPasswordDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PasswordEncoder passwordEncoder;

    private User currentUserOauth(Authentication currentUser) {
        OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) currentUser;

        String username = (String) token.getPrincipal().getAttributes().get("email");
        User u = userDao.findByEmailAndActive(username, true);
        return u;
    }

    private User currentUserUsername(Authentication currentUser) {
        String username = ((UserDetails) currentUser.getPrincipal()).getUsername();
        User u = userDao.findByUsernameAndActive(username,true);
        return u;

    }

    public User checkAuthenticationType(Authentication authentication) {
        if (authentication instanceof OAuth2AuthenticationToken) {
            return currentUserOauth(authentication);
        } else {
            return currentUserUsername(authentication);
        }

    }

    public User createUser(UserDto userDto){
        User user = new User();
        BeanUtils.copyProperties(userDto, user);
        user.setActive(true);
        user.setRole(roleDao.findById(userDto.getRole()).get());
        UserPassword userPassword = new UserPassword();
        userPassword.setUser(user);
        userPassword.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setUserPassword(userPassword);
        userDao.save(user);


        return user;

    }

    public User updateUser(UserDto userDto){
        User user = userDao.findById(userDto.getId()).get();
        BeanUtils.copyProperties(userDto, user);
        user.setActive(true);
        user.setRole(roleDao.findById(userDto.getRole()).get());
        userDao.save(user);

        return user;

    }

    private void createPassword(User user, String password){
        UserPassword userPassword = new UserPassword();
        userPassword.setPassword(passwordEncoder.encode(password));
        userPassword.setUser(user);
        userPasswordDao.save(userPassword);
    }

}
