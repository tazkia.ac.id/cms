package id.ac.tazkia.cms.service;

import id.ac.tazkia.cms.dao.NewsDao;
import id.ac.tazkia.cms.entity.News;
import id.ac.tazkia.cms.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class NewsService {
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private UserService userService;

    public News saveNews(News news, User user){
        News newNews = new News();
        BeanUtils.copyProperties(news, newNews);
        newNews.setCreatedBy(user.getId());
        newsDao.save(newNews);

        log.info("simpan berita berhasil");

        return newNews;
    }

    public News updateNews(News news,User user){
        news.setUpdatedBy(user.getId());
        newsDao.save(news);

        return news;
    }
}
