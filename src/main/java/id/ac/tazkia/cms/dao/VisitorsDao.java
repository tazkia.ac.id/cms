package id.ac.tazkia.cms.dao;

import id.ac.tazkia.cms.entity.Visitors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface VisitorsDao extends CrudRepository<Visitors, String>, PagingAndSortingRepository<Visitors, String> {
    Page<Visitors> findByDeletedIsNullOrderByCreatedDesc(Pageable pageable);

    @Query(value = "select * from visitors where deleted is null and (name like %?1% or email like %?1%)",
            countQuery = "select count(*) from visitors where deleted is null and (name like %?1% or email like %?1%)", nativeQuery = true)
    Page<Visitors> findVisitorsWithSearch(String search, Pageable pageable);
}
