package id.ac.tazkia.cms.dao;

import id.ac.tazkia.cms.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String>, CrudRepository<Role, String> {
}
