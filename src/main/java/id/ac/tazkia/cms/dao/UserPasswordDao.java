package id.ac.tazkia.cms.dao;

import id.ac.tazkia.cms.entity.User;
import id.ac.tazkia.cms.entity.UserPassword;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String>, CrudRepository<UserPassword, String> {
    UserPassword findByUser(User user);
}
