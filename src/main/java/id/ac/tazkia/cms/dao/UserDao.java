package id.ac.tazkia.cms.dao;

import id.ac.tazkia.cms.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends CrudRepository<User, String>, PagingAndSortingRepository<User, String> {
    User findByUsernameAndActive(String email, Boolean active);

    User findByEmailAndActive(String email, Boolean active);

    Page<User> findByActive(Boolean active, Pageable pageable);

    @Query(value = "select * from s_user where active = true and (name like %?1% or email like %?1% or username like %?1%)",
            countQuery = "select count(*) from s_user where active = true and (name like %?1% or email like %?1% or username like %?1%)", nativeQuery = true)
    Page<User> findUsersWithSearch(String search, Pageable pageable);
}
