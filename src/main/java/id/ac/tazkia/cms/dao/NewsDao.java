package id.ac.tazkia.cms.dao;

import id.ac.tazkia.cms.constants.Status;
import id.ac.tazkia.cms.entity.News;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface NewsDao extends PagingAndSortingRepository<News, String>, CrudRepository<News, String> {
    Page<News> findByStatusOrderByPublishDateDesc(Status status, Pageable pageable);
    Optional<News> findBySlug(String slug);

    @Query(value = "select * from news where deleted is null and (title like %?1% or author like %?1%) and status in (?2)",
            countQuery = "select * from news where deleted is null and (title like %?1% or author like %?1%) and status in (?2)", nativeQuery = true)
    Page<News> findNewsWithSearch(String search,List<Status> statusList ,Pageable pageable);

    @Query(value = "select * from news where deleted is null and (title like %?1% or author like %?1%) and status = ?2",
            countQuery = "select * from news where deleted is null and (title like %?1% or author like %?1%) and status = ?2", nativeQuery = true)
    Page<News> findNewsWithSearchAndFilter(String search,Status status ,Pageable pageable);

}
