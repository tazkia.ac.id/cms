import Alpine from "alpinejs";
import collapse from "@alpinejs/collapse";
import mask from "@alpinejs/mask";
import anchor from "@alpinejs/anchor";

Alpine.plugin(collapse);
Alpine.plugin(mask);
Alpine.plugin(anchor);

window.Alpine = Alpine;
Alpine.start();

// form validation
(() => {
	"use strict";

	// Fetch all the forms we want to apply validation styles to
	const forms = document.querySelectorAll("form[novalidate]");

	// Loop over them and prevent submission
	Array.from(forms).forEach((form) => {
		form.addEventListener(
			"submit",
			(event) => {
				if (!form.checkValidity()) {
					event.preventDefault();
					event.stopPropagation();
				}

				const inputs = form.getElementsByClassName("needs-validation");

				Array.from(inputs).forEach((input) => {
					const isValidate = input.checkValidity();
					if (!isValidate) {
						input.setAttribute("data-invalid", "true");

						const errorEl = form.querySelector("#error" + input.name);

						if (errorEl) {
							errorEl.innerHTML = input.validationMessage;
						}
					}
				});
			},
			false,
		);
	});
})();
