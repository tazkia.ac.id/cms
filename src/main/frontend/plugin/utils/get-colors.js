export default function getColors(options, themesColors) {
	let varThemeColors = {};
	let varGlobalColors = {};
	let twConfig = {};
	const variantColors = [];

	const colorOptions = Array.isArray(options.colors) ? options.colors : [options.colors];

	colorOptions.forEach((color, index) => {
		colorOptions[index] = color
			.match(/[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g)
			.join("-")
			.toLowerCase();
	});

	if (options.cssVariable) {
		colorOptions.forEach(function (color, index) {
			if (index == 0 && color in themesColors.cssVar.themes) {
				varThemeColors[":root"] = themesColors.cssVar.themes[color].light;
				if (options.darkMode) varThemeColors[".dark"] = themesColors.cssVar.themes[color].dark;
			} else if (color in themesColors.cssVar.themes) {
				varThemeColors[`.${color}`] = themesColors.cssVar.themes[color].light;
				if (options.darkMode) varThemeColors[`.dark.${color}`] = themesColors.cssVar.themes[color].dark;
			}
		});

		varGlobalColors[":root"] = themesColors.cssVar.globals.light;
		if (options.darkMode) varGlobalColors[".dark"] = themesColors.cssVar.globals.dark;

		twConfig = themesColors.cssVarConfig;
	} else {
		// need to fix
		// extend tailwind color without css variable
		const prymaryColors = themeColors.colorConfig.primary;

		for (const color in themesColors.colorConfig) {
			if (color == "primary") {
				colorOptions.forEach(function (value, index) {
					if (index == 0 && value in prymaryColors) {
						twConfig.primary = prymaryColors[value];
					} else if (value in prymaryColors) {
						twConfig[value] = prymaryColors[value];
						variantColors.push(value);
					}
				});
			} else {
				twConfig[color] = themesColors.colorConfig[color];
			}
		}
	}
	return { variable: { themes: varThemeColors, globals: varGlobalColors }, twConfig: twConfig, variantColors };
}
