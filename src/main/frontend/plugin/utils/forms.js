import svgToDataUri from "mini-svg-data-uri";
import defaultTheme from "tailwindcss/defaultTheme";
import colors from "tailwindcss/colors";
const [baseFontSize, { lineHeight: baseLineHeight }] = defaultTheme.fontSize.base;
const { spacing, borderWidth, borderRadius } = defaultTheme;

function resolveColor(color, opacityVariableName) {
	return color.replace("<alpha-value>", `var(${opacityVariableName}, 1)`);
}

function getFormsStyle(theme) {
	function resolveChevronColor(color, fallback) {
		let resolved = theme(color);

		if (!resolved || resolved.includes("var(")) {
			return fallback;
		}

		return resolved.replace("<alpha-value>", "1");
	}
	return {
		[[
			"[type='text']",
			"[type='email']",
			"[type='url']",
			"[type='password']",
			"[type='number']",
			"[type='date']",
			"[type='datetime-local']",
			"[type='month']",
			"[type='search']",
			"[type='tel']",
			"[type='time']",
			"[type='week']",
			"[multiple]",
			"textarea",
			"select",
		]]: {
			appearance: "none",
			color: resolveColor(theme("colors.on-surface", colors.gray[700]), "--tw-text-opacity"),
			"background-color": resolveColor(theme("colors.surface", colors.white)),
			"border-color": resolveColor(theme("colors.surface-outline", colors.gray[300]), "--tw-border-opacity"),
			"border-width": borderWidth["DEFAULT"],
			"border-radius": borderRadius.none,
			"padding-top": spacing[2],
			"padding-right": spacing[3],
			"padding-bottom": spacing[2],
			"padding-left": spacing[3],
			"font-size": baseFontSize,
			"line-height": baseLineHeight,
			"--tw-shadow": "0 0 #0000",
			"&:focus": {
				outline: "2px solid transparent",
				"outline-offset": "2px",
				"--tw-ring-inset": "var(--tw-empty,/*!*/ /*!*/)",
				"--tw-ring-offset-width": "0px",
				"--tw-ring-offset-color": "#fff",
				"--tw-ring-color": resolveColor(
					theme("colors.primary-outline-highest", colors.blue[500]),
					"--tw-ring-opacity",
				),
				"--tw-ring-offset-shadow": `var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color)`,
				"--tw-ring-shadow": `var(--tw-ring-inset) 0 0 0 calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color)`,
				"box-shadow": `var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow)`,
				"border-color": resolveColor(
					theme("colors.primary-surface-highest", colors.blue[500]),
					"--tw-border-opacity",
				),
			},
		},
		[["input:-webkit-autofill", "textarea:-webkit-autofill", "select:-webkit-autofill"]]: {
			"-webkit-box-shadow": `0 0 0 1000px #ffffff inset !important`,
			"-webkit-text-fill-color": `#1A1B1E !important`,
		},
		[[`.dark input:-webkit-autofill`, `.dark textarea:-webkit-autofill`, `.dark select:-webkit-autofill`]]: {
			"-webkit-box-shadow": `0 0 0 1000px #131416 inset !important`,
			"-webkit-text-fill-color": `#ffffff !important`,
		},
		[["input::placeholder", "textarea::placeholder"]]: {
			color: resolveColor(theme("colors.on-surface-lower", colors.gray[500]), "--tw-text-opacity"),
			opacity: "1",
		},
		["::-webkit-datetime-edit-fields-wrapper"]: {
			padding: "0",
		},
		["::-webkit-date-and-time-value"]: {
			"min-height": "1.5em",
		},
		["::-webkit-datetime-edit"]: {
			display: "inline-flex",
		},
		[[
			"::-webkit-datetime-edit",
			"::-webkit-datetime-edit-year-field",
			"::-webkit-datetime-edit-month-field",
			"::-webkit-datetime-edit-day-field",
			"::-webkit-datetime-edit-hour-field",
			"::-webkit-datetime-edit-minute-field",
			"::-webkit-datetime-edit-second-field",
			"::-webkit-datetime-edit-millisecond-field",
			"::-webkit-datetime-edit-meridiem-field",
		]]: {
			"padding-top": 0,
			"padding-bottom": 0,
		},

		["select"]: {
			"background-image": `url("${svgToDataUri(
				`<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20"><path stroke="${resolveChevronColor(
					"colors.on-surface-lower",
					colors.gray[500],
				)}" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M6 8l4 4 4-4"/></svg>`,
			)}")`,
			"background-position": `right ${spacing[2]} center`,
			"background-repeat": `no-repeat`,
			"background-size": `1.5em 1.5em`,
			"padding-right": spacing[10],
			"print-color-adjust": `exact`,
		},
		[".dark select"]: {
			"background-image": `url("${svgToDataUri(
				`<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20"><path stroke="${resolveChevronColor(
					"colors.on-surface-lower",
					colors.gray[500],
				)}" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M6 8l4 4 4-4"/></svg>`,
			)}")`,
		},
		[[`:is([dir=rtl]) select`]]: {
			backgroundPosition: `left ${spacing[3]} center`,
			paddingRight: spacing[3],
			paddingLeft: 0,
		},
		[["[multiple]", '[size]:where(select:not([size="1"]))']]: {
			"background-image": "initial",
			"background-position": "initial",
			"background-repeat": "unset",
			"background-size": "initial",
			"padding-right": spacing[3],
			"print-color-adjust": "unset",
		},
		[[`.dark [multiple]`, '[size]:where(select:not([size="1"]))']]: {
			"background-image": "initial",
		},
		[[`[type='checkbox']`, `[type='radio']`]]: {
			appearance: "none",
			padding: "0",
			"print-color-adjust": "exact",
			display: "inline-block",
			"vertical-align": "middle",
			"background-origin": "border-box",
			"user-select": "none",
			"flex-shrink": "0",
			height: spacing[4],
			width: spacing[4],
			color: resolveColor(theme("colors.primary", colors.blue[500]), "--tw-text-opacity"),
			"background-color": "#fff",
			"border-color": resolveColor(theme("colors.surface-outline", colors.gray[300]), "--tw-border-opacity"),
			"border-width": borderWidth["DEFAULT"],
			"--tw-shadow": "0 0 #0000",
		},
		[`[type='checkbox']`]: {
			"border-radius": borderRadius["none"],
		},
		[`[type='radio']`]: {
			"border-radius": "100%",
		},
		[[`[type='checkbox']:focus`, `[type='radio']:focus`]]: {
			outline: "2px solid transparent",
			"outline-offset": "2px",
			"--tw-ring-inset": "var(--tw-empty,/*!*/ /*!*/)",
			"--tw-ring-offset-width": "2px",
			"--tw-ring-offset-color": theme("colors.surface", colors.white),
			"--tw-ring-color": resolveColor(
				theme("colors.primary-outline-highest", colors.blue[500]),
				"--tw-ring-opacity",
			),
			"--tw-ring-offset-shadow": `var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color)`,
			"--tw-ring-shadow": `var(--tw-ring-inset) 0 0 0 calc(2px + var(--tw-ring-offset-width)) var(--tw-ring-color)`,
			"box-shadow": `var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow)`,
		},
		[[
			`[type='checkbox']:checked`,
			`[type='radio']:checked`,
			`.dark [type='checkbox']:checked`,
			`.dark [type='radio']:checked`,
		]]: {
			"border-color": `transparent`,
			"background-color": `currentColor`,
			"background-size": `0.55em 0.55em`,
			"background-position": `center`,
			"background-repeat": `no-repeat`,
		},
		[`[type='checkbox']:checked`]: {
			"background-image": `url("${svgToDataUri(
				`<svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 12">
                         <path stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" d="M1 5.917 5.724 10.5 15 1.5"/>
                     </svg>`,
			)}")`,
			"background-repeat": `no-repeat`,
			"background-size": `0.55em 0.55em`,
			"print-color-adjust": `exact`,
		},
		[`[type='radio']:checked`]: {
			"background-image": `url("${svgToDataUri(
				`<svg viewBox="0 0 16 16" fill="white" xmlns="http://www.w3.org/2000/svg"><circle cx="8" cy="8" r="3"/></svg>`,
			)}")`,
			"background-size": `1em 1em`,
		},
		[`.dark [type='radio']:checked`]: {
			"background-image": `url("${svgToDataUri(
				`<svg viewBox="0 0 16 16" fill="white" xmlns="http://www.w3.org/2000/svg"><circle cx="8" cy="8" r="3"/></svg>`,
			)}")`,
			"background-size": `1em 1em`,
		},
		[[
			`[type='checkbox']:checked:hover`,
			`[type='checkbox']:checked:focus`,
			`[type='radio']:checked:hover`,
			`[type='radio']:checked:focus`,
		]]: {
			"border-color": "transparent",
			"background-color": "currentColor",
		},
		[`[type='checkbox']:indeterminate`]: {
			"background-image": `url("${svgToDataUri(
				`<svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 12">
                     <path stroke="white" stroke-linecap="round" stroke-linejoin="round" stroke-width="3" d="M0.5 6h14"/>
                     </svg>`,
			)}")`,
			"background-color": `currentColor`,
			"border-color": `transparent`,
			"background-position": `center`,
			"background-repeat": `no-repeat`,
			"background-size": `0.55em 0.55em`,
			"print-color-adjust": `exact`,
		},
		[[`[type='checkbox']:indeterminate:hover`, `[type='checkbox']:indeterminate:focus`]]: {
			"border-color": "transparent",
			"background-color": "currentColor",
		},
		[`[type='file']`]: {
			background: "unset",
			"border-color": "inherit",
			"border-width": "0",
			"border-radius": "0",
			padding: "0",
			"font-size": "unset",
			"line-height": "inherit",
		},
		[`[type='file']:focus`]: {
			outline: `1px auto inherit`,
		},
		[[`input[type=file]::file-selector-button`]]: {
			color: resolveColor(theme("colors.on-primary", colors.white), "--tw-text-opacity"),
			background: resolveColor(theme("colors.primary", colors.blue[500]), "--tw-background-opacity"),
			border: 0,
			"font-weight": theme("fontWeight.medium"),
			"font-size": theme("fontSize.sm"),
			cursor: "pointer",
			"padding-top": spacing[2.5],
			"padding-bottom": spacing[2.5],
			"padding-left": spacing[8],
			"padding-right": spacing[4],
			"margin-inline-start": "-1rem",
			"margin-inline-end": "1rem",
			"&:hover": {
				background: resolveColor(theme("colors.primary-higher", colors.blue[600]), "--tw-background-opacity"),
			},
		},
		[[`:is([dir=rtl]) input[type=file]::file-selector-button`]]: {
			paddingRight: spacing[8],
			paddingLeft: spacing[4],
		},
		[[`.dark input[type=file]::file-selector-button`]]: {
			color: resolveColor(theme("colors.on-surface-container", colors.gray[100]), "--tw-text-opacity"),
			background: resolveColor(
				theme("colors.surface-container-higher", colors.gray[600]),
				"--tw-background-opacity",
			),
			"&:hover": {
				background: resolveColor(
					theme("colors.surface-container-highest", colors.gray[500]),
					"--tw-background-opacity",
				),
			},
		},
		[[`input[type="range"]::-webkit-slider-thumb`]]: {
			height: spacing[5],
			width: spacing[5],
			background: resolveColor(theme("colors.primary", colors.blue[500]), "--tw-background-opacity"),
			"border-radius": borderRadius.full,
			border: 0,
			appearance: "none",
			"-moz-appearance": "none",
			"-webkit-appearance": "none",
			cursor: "pointer",
		},
		[[`input[type="range"]:disabled::-webkit-slider-thumb`]]: {
			background: resolveColor(
				theme("colors.surface-container-highest", colors.gray[400]),
				"--tw-background-opacity",
			),
		},
		[[`.dark input[type="range"]:disabled::-webkit-slider-thumb`]]: {
			background: resolveColor(
				theme("colors.surface-container-highest", colors.gray[500]),
				"--tw-background-opacity",
			),
		},
		[[`input[type="range"]:focus::-webkit-slider-thumb`]]: {
			outline: "2px solid transparent",
			"outline-offset": "2px",
			"--tw-ring-offset-shadow":
				"var(--tw-ring-inset) 0 0 0 var(--tw-ring-offset-width) var(--tw-ring-offset-color)",
			"--tw-ring-shadow":
				"var(--tw-ring-inset) 0 0 0 calc(4px + var(--tw-ring-offset-width)) var(--tw-ring-color)",
			"box-shadow": "var(--tw-ring-offset-shadow), var(--tw-ring-shadow), var(--tw-shadow, 0 0 #0000)",
			"--tw-ring-opacity": 1,
			"--tw-ring-color": "rgb(164 202 254 / var(--tw-ring-opacity))",
		},
		[[`input[type="range"]::-moz-range-thumb`]]: {
			height: spacing[5],
			width: spacing[5],
			background: resolveColor(theme("colors.primary", colors.blue[500]), "--tw-background-opacity"),
			"border-radius": borderRadius.full,
			border: 0,
			appearance: "none",
			"-moz-appearance": "none",
			"-webkit-appearance": "none",
			cursor: "pointer",
		},
		[[`input[type="range"]:disabled::-moz-range-thumb`]]: {
			background: resolveColor(
				theme("colors.surface-container-highest", colors.gray[400]),
				"--tw-background-opacity",
			),
		},
		[[`.dark input[type="range"]:disabled::-moz-range-thumb`]]: {
			background: resolveColor(
				theme("colors.surface-container-highest", colors.gray[500]),
				"--tw-background-opacity",
			),
		},
		[[`input[type="range"]::-moz-range-progress`]]: {
			background: resolveColor(theme("colors.primary", colors.blue[500]), "--tw-background-opacity"),
		},
		[[`input[type="range"]::-ms-fill-lower`]]: {
			background: resolveColor(theme("colors.primary", colors.blue[500]), "--tw-background-opacity"),
		},
		[[`input[type="range"].thumb-sm::-webkit-slider-thumb`]]: {
			height: spacing[4],
			width: spacing[4],
		},
		[[`input[type="range"].thumb-lg::-webkit-slider-thumb`]]: {
			height: spacing[6],
			width: spacing[6],
		},
		[[`input[type="range"].thumb-sm::-moz-range-thumb`]]: {
			height: spacing[4],
			width: spacing[4],
		},
		[[`input[type="range"].thumb-lg::-moz-range-thumb`]]: {
			height: spacing[6],
			width: spacing[6],
		},
	};
}

export default getFormsStyle;
