import plugin from "tailwindcss/plugin";
import getFormsStyle from "./utils/forms";
import getColors from "./utils/get-colors";
import themesColors from "./utils/themes-colors";
import { root } from "postcss";

const defaultColor = ["azure-radiance"];

const winfativePlugin = plugin.withOptions(
	function (options = {}) {
		const { forms = true, colors = defaultColor, cssVariable = true, darkMode = true } = options;

		const customColors = getColors({ colors: colors, cssVariable: cssVariable, darkMode: darkMode }, themesColors);

		return function ({ addBase, addVariant, theme }) {
			if (cssVariable) {
				addBase(customColors.variable.themes);
				addBase(customColors.variable.globals);
			}

			if (forms) addBase(getFormsStyle(theme));

			addBase({
				":root": {
					"--default-border-radius": "0.375rem",
				},
			});

			if (!cssVariable && customColors.variantColors.length > 0) {
				customColors.variantColors.forEach(function (value) {
					addVariant(`${value}`, `.${value} &`);
				});
			}
		};
	},
	function (options = {}) {
		const { colors = defaultColor, cssVariable = true, darkMode = true } = options;
		const customColors = getColors({ colors: colors, cssVariable: cssVariable, darkMode: darkMode }, themesColors);
		return {
			darkMode: "class",
			theme: {
				container: {
					center: true,
				},
				extend: {
					colors: customColors.twConfig,
					borderRadius: {
						default: "var(--default-border-radius)",
					},
				},
			},
		};
	},
);

export default winfativePlugin;
