alter table news
    add column publish_date date,
    add column author varchar(100),
    add column status varchar(50);

ALTER TABLE news
    CHANGE COLUMN `slug` `slug` LONGTEXT NULL DEFAULT NULL ;

alter table s_user
    add column name varchar(100);
