create table s_user_password
(
    id       varchar(36),
    id_user  varchar(36)  not null,
    password varchar(255) not null,
    primary key (id)
);

INSERT INTO s_permission (id, permission_value, permission_label)
VALUES ('editadmin', 'EDIT_ADMIN', 'Edit Admin'),
       ('viewadmin', 'VIEW_ADMIN', 'View Admin');

INSERT INTO s_role (id, description, name)
VALUES ('admin', 'ADMIN', 'Admin');

INSERT INTO s_role_permission (id_role, id_permission)
VALUES ('admin', 'editadmin'),
       ('admin', 'viewadmin');
