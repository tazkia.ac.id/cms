CREATE TABLE s_permission
(
    id               VARCHAR(255) NOT NULL,
    permission_label VARCHAR(255) NOT NULL,
    permission_value VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (permission_value)
);

CREATE TABLE s_role
(
    id          VARCHAR(255) NOT NULL,
    description VARCHAR(255) DEFAULT NULL,
    name        VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (name)
);

CREATE TABLE s_role_permission
(
    id_role       VARCHAR(255) NOT NULL,
    id_permission VARCHAR(255) NOT NULL,
    PRIMARY KEY (id_role, id_permission),
    FOREIGN KEY (id_permission) REFERENCES s_permission (id),
    FOREIGN KEY (id_role) REFERENCES s_role (id)
);

CREATE TABLE s_user
(
    id       VARCHAR(36),
    username VARCHAR(255) NOT NULL,
    email    VARCHAR(50)  NOT NULL,
    active   BOOLEAN      NOT NULL,
    id_role  VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (username),
    FOREIGN KEY (id_role) REFERENCES s_role (id)
);

CREATE TABLE s_user_role
(
    id_user VARCHAR(36) not null,
    id_role VARCHAR(36) NOT NULL,
    PRIMARY KEY (id_user, id_role),
    FOREIGN KEY (id_user) REFERENCES s_user (id),
    FOREIGN KEY (id_role) REFERENCES s_role (id)
);


create table news
(
    id         varchar(50) not null,
    created    timestamp(6),
    created_by varchar(100),
    updated    timestamp(6),
    updated_by varchar(100),
    deleted    timestamp(6),
    thumbnail  varchar(100),
    slug       varchar(100),
    title      longtext,
    subtitle   longtext,
    content    longtext,
    primary key (id)
);

create table visitors
(
    id         varchar(50) not null,
    created    timestamp(6),
    created_by varchar(100),
    updated    timestamp(6),
    updated_by varchar(100),
    deleted    timestamp(6),
    name       varchar(50),
    email      varchar(50),
    phone      varchar(20),
    message    longtext,
    primary key (id)
);

